'''
Author: Maxime MOREILLON
'''

from flask import Flask, escape, request, jsonify
from flask_cors import CORS
from dotenv import load_dotenv
import os
import numpy as np
import json
import tensorflow as tf
from tensorflow import keras
import time

load_dotenv()

# Environment variables
MODEL_VERSION = os.getenv("MODEL_VERSION") or 'none'
MODEL_NAME = os.getenv("MODEL_NAME") or 'none'

# Loading the AI model
MODEL_FOLDER_NAME = "model"
model_loaded = False
try:
    model = keras.models.load_model(MODEL_FOLDER_NAME)
    model_loaded = True
    print('AI model loaded')
except Exception as e:
    print('Failed to load the AI model')

# Flask app
app = Flask(__name__)
CORS(app)

@app.route('/', methods=['GET'])
def home():

    return jsonify( {
    'applicationname': 'AI model server',
    'author': 'Maxime MOREILLON',
    'version': '1.0.1',
    'model_loaded': model_loaded,
    } )

@app.route('/predict', methods=['POST'])
def predict():

    instances = request.json['instances']

    # This is very slow and needs to be improved
    model_input = np.array(instances)

    inference_start = time.time()

    # AI Prediction
    try:
        model_output = model(model_input)
    except Exception as e:
        message = 'AI prediction failed: {}'.format(e)
        print(message)
        return message, 500

    inference_time = time.time() - inference_start

    # converting output to numpy array
    model_output_numpy = model_output.numpy()

    return jsonify( {
    'predictions': model_output_numpy.tolist(),
    'model_version': MODEL_VERSION,
    'model_name': MODEL_NAME,
    'inference_time': inference_time,
    } )



if __name__ == '__main__':
    app.run('0.0.0.0',8566)
